#pragma once

#include "electricfield.h"
#include "functions_for_density_operator.h"

struct adams_step_t1
    : public std::binary_function<cPrecision, cPrecision, cPrecision>
{
  cPrecision operator()(cPrecision a, cPrecision b)
  {
    return (cPrecision(3, 0) * a + b) / 2.0;
  }
};

struct adams_step_t2
    : public std::binary_function<cPrecision, cPrecision, cPrecision>
{
  cPrecision operator()(cPrecision a, cPrecision b)
  {
    return a * b;
  }
};

struct euler_step_t1
    : public std::binary_function<cPrecision, cPrecision, cPrecision>
{
  cPrecision operator()(cPrecision a, cPrecision b)
  {
    return a * b;
  }
};

// defines a density operator for a given particle
class density_operator
{
 public:
  int levels;  // number of levels of the atomic system

  ublas::vector<cPrecision>
      rhos_j;  // upper triangular rho_ij flatten across the last dimension of
               // the array
               // {rho_11,rho_12,...rho_1lvels,rho_22,...,rho_levelslevels}-

  ublas::vector<cPrecision>
      last_rho_j;  // auxiliar vector for adams_predictor method

  int dim_levels;  // size of the vector, corresponding to the independent
                   // variables levels(levels-1)/2

  cPrecision b_alpha;  // a free scaling parameter for light-matter interaction

  // density_operator constructor
  density_operator(int levels) : levels(levels)
  {
    // dim levels
    dim_levels = levels * (levels - 1) / 2 + levels;
    // initialize the array to store the rho_ij
    rhos_j = ublas::vector<cPrecision>(dim_levels, cPrecision(0, 0));
    initialize();
  }

  // method to initialize the density operator to the ground state
  void initialize()
  {
    // set all atoms to the ground state
    rhos_j[0] = cPrecision(1.0, 0.0);
  };

  // method to initialize the density operator to a given state
  void initialize(ublas::vector<cPrecision> rho_0)
  {
    // set all atoms to the ground state
    rhos_j = rho_0;
  };

  // void save_rhos(std::string saveDir);

  // void load_rhos(std::string loadDir, int step);

  // void push_0(af::array envelope);

  // adams-bashforth method - compute the step

  ublas::vector<cPrecision> adams_function(vector<ElectricField> EMFs,
                                           double x,
                                           double y,
                                           double z,
                                           double time,
                                           double dt,
                                           double vx = 0,
                                           double vy = 0,
                                           double vz = 0)
  {
    ublas::vector<cPrecision> k_1;
    k_1 = rho_update(rhos_j, EMFs, dim_levels, x, y, z, time, vx, vy, vz);
    ublas::vector<cPrecision> k_2 = last_rho_j;

    last_rho_j = k_1;

    return (dt / 2.0) * (3 * k_1 - k_2);
  };

  // vector<cPrecision> rk4_function(af::array envelope, int index, Precision
  // dt);

  ublas::vector<cPrecision> rk4_function(vector<ElectricField> EMFs,
                                         double x,
                                         double y,
                                         double z,
                                         double time,
                                         double dt,
                                         double vx = 0,
                                         double vy = 0,
                                         double vz = 0)
  {
    ublas::vector<cPrecision> k_1 =
        rho_update(rhos_j, EMFs, dim_levels, x, y, z, time, vx, vy, vz);
    ublas::vector<cPrecision> k_2 = rho_update(
        rhos_j + k_1 / 2.0, EMFs, dim_levels, x, y, z, time, vx, vy, vz);
    ublas::vector<cPrecision> k_3 = rho_update(
        rhos_j + k_2 / 2.0, EMFs, dim_levels, x, y, z, time, vx, vy, vz);
    ublas::vector<cPrecision> k_4 =
        rho_update(rhos_j + k_3, EMFs, dim_levels, x, y, z, time, vx, vy, vz);

    return (dt / 6.) * (k_1 + 2. * k_2 + 2. * k_3 + k_4);
  }

  ublas::vector<cPrecision> euler_function(vector<ElectricField> EMFs,
                                           double x,
                                           double y,
                                           double z,
                                           double time,
                                           double dt,
                                           double vx = 0,
                                           double vy = 0,
                                           double vz = 0)
  {
    ublas::vector<cPrecision> k_1 =
        rho_update(rhos_j, EMFs, dim_levels, x, y, z, time, vx, vy, vz);

    return (dt) * (k_1);
  }

  void push_euler(vector<ElectricField> EMFs,
                  double x,
                  double y,
                  double z,
                  double time,
                  double dt,
                  double vx = 0,
                  double vy = 0,
                  double vz = 00)
  {
    ublas::vector<cPrecision> k_1 =
        rho_update(rhos_j, EMFs, dim_levels, x, y, z, time, vx, vy, vz);
    rhos_j += (dt * k_1);
    // rhos_j += euler_function(EMFvalue, dt, vx, vy, vz);
  }

  void push_adams(vector<ElectricField> EMFs,
                  double x,
                  double y,
                  double z,
                  double time,
                  double dt,
                  double vx = 0,
                  double vy = 0,
                  double vz = 0)
  {
    rhos_j += adams_function(EMFs, x, y, z, time, dt, vx, vy, vz);
  }

  void push_rk4(vector<ElectricField> EMFs,
                double x,
                double y,
                double z,
                double time,
                double dt,
                double vx = 0,
                double vy = 0,
                double vz = 0)
  {
    rhos_j += rk4_function(EMFs, x, y, z, time, dt, vx, vy, vz);
  }

  cPrecision trace()
  {
    cPrecision trac = 0;

    for (int i = 1; i <= levels; i++) {
      trac += rhos_j[ij_to_I(i, i, levels)];
    }

    return trac;
  }
};
