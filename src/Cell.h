#pragma once
/*

 QPARTICLES:
 a code to simulate many-body quantum particles in
 interaction with electromagnetic fiels
 in massively parallel HPC environments

 Nuno Azevedo Silva
 nuno.a.silva@inesctec.pt

 -------------------------------------------------------

 Cell.h
 include a class for creating a cell, containing particles

 */

#include "particle.h"

// Class for keeping track the contents of a single Cell
class Cell {

public:

	vector<Particle> particles;		//vector containing the particles

	double Cell_lx;					//cell dimension - x direction
	double Cell_ly;					//cell dimension - y direction
	double Cell_lz;					//cell dimension - z direction

	int x_pos_abs;					//position in absolute map of cells
	int y_pos_abs;					//position in absolute map for cells
	int z_pos_abs;

	// Buffer: vector to store the coordinates received from each cell around it
	double **remote_particles_middle_plane;
	double **remote_particles_top_plane;
	double **remote_particles_bottom_plane;

	// Buffer to hold coordinates to send
	vector<double> PartBuff_mp;
	vector<double> PartBuff_tp;
	vector<double> PartBuff_bp;


	//receive all the information from 26 neighboors communications, plane by plane
	//middle plane requests
	MPI_Request reqr_middle_plane[8], reqs_middle_plane[8];
	int nreqr_middle_plane, nreqs_middle_plane;

	//top plane requests
	MPI_Request reqr_top_plane[9], reqs_top_plane[9];
	int nreqr_top_plane, nreqs_top_plane;

	//bottom plane requests
	MPI_Request reqr_bottom_plane[9], reqs_bottom_plane[9];
	int nreqr_bottom_plane, nreqs_bottom_plane;

	//number of particles
	int nparticles;

	//first step
	bool FirstStep;


	//default constructor
	Cell(int i, int j, int k, int Node_x, int Node_y, int Node_z, int Nx_per_node, int Ny_per_node, int Nz_per_node, int nParticles, double Cell_lx, double Cell_ly, double Cell_lz,int nlevels = NLEVELS) :
		particles(nParticles,Particle(NLEVELS)), Cell_lx(Cell_lx), Cell_ly(Cell_ly), Cell_lz(Cell_lz), nparticles(nParticles)
	{
		FirstStep = true;

		//compute the absolute position of the cell in the system overall
		x_pos_abs = Node_x*Nx_per_node + i;
		y_pos_abs = Node_y*Ny_per_node + j;
		z_pos_abs = Node_z*Nz_per_node + k;

		//  Filling the cell		
		double boundx = Cell_lx * double(i) + Cell_lx * Nx_per_node* Node_x;
		double boundy = Cell_ly * double(j) + Cell_ly * Ny_per_node* Node_y;
		double boundz = Cell_lz * double(k) + Cell_lz * Nz_per_node* Node_z;

		double dx = Cell_lx;
		double dy = Cell_ly;
		double dz = Cell_lz;

		double n_3 = pow(nParticles, 1. / 3.);

		//distribute them randomly, with random velocity for now but should be updated to a velocity distribution
		for (int p = 0; p < particles.size(); p++)
		{
			particles[p] = Particle(NLEVELS);
			particles[p].x = boundx + ((float(rand()) / RAND_MAX)*(dx));
			particles[p].y = boundy + ((float(rand()) / RAND_MAX)*(dy));
			particles[p].z = boundz + ((float(rand()) / RAND_MAX)*(dz));
			particles[p].vx = ((float(rand()) / RAND_MAX - 0.5))*0.01;
			particles[p].vy = ((float(rand()) / RAND_MAX - 0.5))*0.01;
			particles[p].vz = ((float(rand()) / RAND_MAX - 0.5))*0.01;
		}


		// Allocate the space for neighboor particles
		remote_particles_middle_plane = (double**)malloc(sizeof(double*) * 8);
		for (int i = 0; i < 8; i++)
			remote_particles_middle_plane[i] = (double *)calloc(sizeof(double), nparticles * 12 + 1);

		remote_particles_top_plane = (double**)malloc(sizeof(double*) * 9);
		for (int i = 0; i < 9; i++)
			remote_particles_top_plane[i] = (double *)calloc(sizeof(double), nparticles * 12 + 1);

		remote_particles_bottom_plane = (double**)malloc(sizeof(double*) * 9);
		for (int i = 0; i < 9; i++)
			remote_particles_bottom_plane[i] = (double *)calloc(sizeof(double), nparticles * 12 + 1);


	}

	void pack_message()
	{

		for (int i = 0; i < nparticles * 12 + 1; i++)
		{
			PartBuff_mp.push_back(0); //allocate the necessary for nParticles * 2
		}


		PartBuff_mp[0] = particles.size();
		for (int i = 0; i < particles.size(); i++)
		{
			PartBuff_mp[1 + i * 3] = particles[i].x;
			PartBuff_mp[1 + i * 3 + 1] = particles[i].y;
			PartBuff_mp[1 + i * 3 + 2] = particles[i].z;
		}

	}

	//communicate the middle plane cells and receive them
	void Communicate_middle_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {

		
		
		if (FirstStep == false)
		{
			for (int i = 0; i < 8; i++)
				free(remote_particles_middle_plane[i]);
			free(remote_particles_middle_plane);
		}
		

		
		// Receive calls
		// i+1,j,k
		int ircount = 0;
		
		
		if (x_pos_abs != Dimensionx - 1)
		{   
			int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		
		//i-1,j,k
		if (x_pos_abs != 0) 
		{
			int tag = CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles*12, MPI_DOUBLE, CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}

		
		// i,j+1,k
		if (y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles*12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		// i,j-1,k
		if (y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles*12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		
		// i+1,j+1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs + 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		//i+1,j-1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		//i-1,j+1,k
		if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs - 1][y_pos_abs+1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		//i-1,j-1,k
		if (x_pos_abs != 0 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs][1];
			MPI_Irecv(remote_particles_middle_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
			ircount++;
		}
		/**/

		
		
		// Now the sending
		int iscount = 0;

		
		//i+1,j,k
		if (x_pos_abs != Dimensionx - 1) 
		{
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
			
		}
		
		// i-1,j,k
		if (x_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}

		
		
		// i,j+1,k
		if (y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		
		// i,j-1,k
		if (y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		
		
		// i+1,j+1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs + 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		//i+1,j-1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs - 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		//i-1,j+1,k
		if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		//i-1,j-1,k
		if (x_pos_abs != 0 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs][0], tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
			iscount++;
		}
		/**/

		nreqr_middle_plane = ircount;
		nreqs_middle_plane = iscount;
		/**/

		//PartBuff_mp.clear();
		//PartBuff_mp.shrink_to_fit();
		

	}

	//communicate the top plane cells
	void Communicate_top_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {


		// Allocate the space for neighboor particles
		
		if (FirstStep == false)
		{
			for (int i = 0; i < 9; i++) 
				free(remote_particles_top_plane[i]);
			free(remote_particles_top_plane);
		}

	

		//Allocate the space for buffer
		/*
		for (int i = 0; i < nparticles * 12; i++)
		{
			PartBuff_tp.push_back(0); //allocate the necessary for nParticles * 2
		}

		// Pack the message

		PartBuff_tp[0] = particles.size();
		for (int i = 0; i < particles.size(); i++)
		{
			PartBuff_tp[1 + i * 3] = particles[i].x;
			PartBuff_tp[1 + i * 3 + 1] = particles[i].y;
			PartBuff_tp[1 + i * 3 + 2] = particles[i].z;
		}

		int nparticles = particles.size();
		/**/

		
		int ircount = 0;
		int iscount = 0;
		
		if (z_pos_abs != Dimensionz - 1)
		{
			
			// Receive calls
			// i+1,j,k+1
			
			if (x_pos_abs != Dimensionx - 1)
			{  
				int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, 
					CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs + 1][0], tag,
					 MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			
			//i-1,j,k+1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			// i,j+1,k+1
			if (y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			// i,j-1,k+1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			// i+1,j+1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			//i+1,j-1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			//i-1,j+1,k+1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			//i-1,j-1,k+1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}
			//i,j,k+1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs + 1][1];
				MPI_Irecv(remote_particles_top_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
				ircount++;
			}

			// Now the sending
			//i+1,j,k+1
			if (x_pos_abs != Dimensionx - 1)
			{
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			// i-1,j,k+1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			// i,j+1,k+1
			if (y_pos_abs != Dimensiony-1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			// i,j-1,k+1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			// i+1,j+1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			//i+1,j-1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			//i-1,j+1,k+1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			//i-1,j-1,k+1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}

			//i,j,k+1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs][z_pos_abs + 1][0], tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
				iscount++;
			}
			/**/
		}

		nreqr_top_plane = ircount;
		nreqs_top_plane = iscount;

		//PartBuff_tp.clear();
		//PartBuff_tp.shrink_to_fit();
	}

	//communicate the bottom plane cells
	void Communicate_bottom_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {


		if (FirstStep == false)
		{
			for (int i = 0; i < 9; i++)
				free(remote_particles_bottom_plane[i]);
			free(remote_particles_bottom_plane);
		}


		//Allocate the space for buffer
		/*
		for (int i = 0; i < nparticles * 12; i++)
		{
			PartBuff_bp.push_back(0); //allocate the necessary for nParticles * 2
		}


		// Pack the message

		PartBuff_bp[0] = particles.size();
		for (int i = 0; i < particles.size(); i++)
		{
			PartBuff_bp[1 + i * 3] = particles[i].x;
			PartBuff_bp[1 + i * 3 + 1] = particles[i].y;
			PartBuff_bp[1 + i * 3 + 2] = particles[i].z;
		}

		int nparticles = particles.size();
		/**/



		int ircount = 0;
		int iscount = 0;
		if (z_pos_abs != 0)
		{
			// Receive calls
			// i+1,j,k-1
			if (x_pos_abs != Dimensionx - 1)
			{   
				int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			//i-1,j,k-1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			// i,j+1,k-1
			if (y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			// i,j-1,k-1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			// i+1,j+1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs+1][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			//i+1,j-1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			//i-1,j+1,k-1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			//i-1,j-1,k-1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}
			//i,j,k-1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][1];
				MPI_Irecv(remote_particles_bottom_plane[ircount], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
				ircount++;
			}

			// Now the sending

			//i+1,j,k-1
			if (x_pos_abs != Dimensionx - 1)
			{
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			// i-1,j,k-1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			// i,j+1,k-1
			if (y_pos_abs != Dimensiony -1 ) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			// i,j-1,k-1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			// i+1,j+1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			//i+1,j-1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			//i-1,j+1,k-1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
			//i-1,j-1,k-1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}

			//i,j,k-1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][0], tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
				iscount++;
			}
		}

		nreqr_bottom_plane = ircount;
		nreqs_bottom_plane = iscount;

		//PartBuff_bp.clear();
		//PartBuff_bp.shrink_to_fit();
	}

	//wait for middle plane comms
	void PostWaits_middle_plane(void) {
		MPI_Status statr[8];
		MPI_Status stats[8];
		MPI_Waitall(nreqs_middle_plane, reqs_middle_plane, stats);
		MPI_Waitall(nreqr_middle_plane, reqr_middle_plane, statr);

	}

	//wati for top plane comms
	void PostWaits_top_plane(void) {
		MPI_Status statr[9];
		MPI_Status stats[9];
		MPI_Waitall(nreqs_top_plane, reqs_top_plane, stats);
		MPI_Waitall(nreqr_top_plane, reqr_top_plane, statr);
	}

	//wait for bottom plane comms
	void PostWaits_bottom_plane(void) {
		MPI_Status statr[9];
		MPI_Status stats[9];
		MPI_Waitall(nreqs_bottom_plane, reqs_bottom_plane, stats);
		MPI_Waitall(nreqr_bottom_plane, reqr_bottom_plane, statr);
		//FirstStep = false;
		PartBuff_mp.clear();
		PartBuff_mp.shrink_to_fit();
		
	}

};

