#pragma once

#include "misc.h"

#define NLEVELS 3
#define DIMLEVELS (NLEVELS * (NLEVELS - 1)) / 2 + NLEVELS

// define an update function for the density operator - use mathematica script

// three level atom
ublas::vector<cPrecision> rho_update(ublas::vector<cPrecision> rhos,
                                     vector<ElectricField> EMFs,
                                     int dim_levels,
                                     double x,
                                     double y,
                                     double z,
                                     double t,
                                     double vx = 0,
                                     double vy = 0,
                                     double vz = 0)
{
  // add an exception to test the number of equations

  ublas::vector<cPrecision> rho_aux(dim_levels, cPrecision(0, 0));

  double dc = 0.0;
  double dp;

  if (x < 1.0 * 10e2) {
    dp = 0.01;
  } else {
    dp = -0.01;
  }

  double g   = 0.5;
  double g3  = 0.5;
  double g2  = 0.5;
  double Oc  = 0 * 0.5;
  double Occ = 0 * 0.5;

  cPrecision EMFvalue = EMFs[0].Value(x, y, z, t) + EMFs[1].Value(x, y, z, t);

  /*
  rho_aux[0] = (cPrecision(0, -1.0))*EMFvalue*rhos[1] +
  cPrecision(0, 1.0)*conj(EMFvalue)*conj(rhos[1]) + g * rhos[2]; rho_aux[1] =
  (cPrecision(0, -1.0))*conj(EMFvalue)*rhos[0] + (-g / 2 +
  cPrecision(0, 1.0)*(-dp))*rhos[1] + cPrecision(0, 1.0)*conj(EMFvalue)*rhos[2];
  rho_aux[2] = cPrecision(0, 1.0)*EMFvalue*rhos[1] -
  cPrecision(0, 1.0)*conj(EMFvalue)*conj(rhos[1]) - g * rhos[2]; rho_aux[3] = 0;
  rho_aux[4] = 0;
  rho_aux[5] = 0;
  */

  rho_aux[0] = (cPrecision(0, -1.0)) * EMFvalue * rhos[2] +
               cPrecision(0, 1.0) * conj(EMFvalue) * conj(rhos[2]) +
               g3 * rhos[5];
  rho_aux[1] = (cPrecision(0, 1.0) * (dc - dp) - g2 / 2) * rhos[1] -
               cPrecision(0, 1.0) * Oc * rhos[2] +
               cPrecision(0, 1.0) * conj(EMFvalue) * conj(rhos[4]);
  rho_aux[2] = (cPrecision(0, -1.0)) * conj(EMFvalue) * rhos[0] -
               cPrecision(0, 1.0) * Occ * rhos[1] +
               ((cPrecision(0, -1.0)) * dp - g3) * rhos[2] +
               cPrecision(0, 1.0) * conj(EMFvalue) * rhos[5];
  rho_aux[3] = (cPrecision(0, -1.0)) * Oc * rhos[4] +
               cPrecision(0, 1.0) * Occ * conj(rhos[4]) + g3 * rhos[5];
  rho_aux[4] = (cPrecision(0, -1.0)) * conj(EMFvalue) * conj(rhos[1]) -
               cPrecision(0, 1.0) * Occ * rhos[3] +
               ((cPrecision(0, -1.0)) * dc - g3) * rhos[4] +
               cPrecision(0, 1.0) * Occ * rhos[5];
  rho_aux[5] = cPrecision(0, 1.0) * EMFvalue * rhos[2] +
               cPrecision(0, 1.0) * Oc * rhos[4] -
               cPrecision(0, 1.0) * conj(EMFvalue) * conj(rhos[2]) -
               cPrecision(0, 1.0) * Occ * conj(rhos[4]) - 2 * g3 * rhos[5];

  return rho_aux;
}

// two level atom
ublas::vector<cPrecision> rho_update_tl(ublas::vector<cPrecision> rhos,
                                        cPrecision EMFvalue,
                                        int dim_levels,
                                        double vx = 0,
                                        double vy = 0,
                                        double vz = 0)
{
  // add an exception to test the number of equations

  ublas::vector<cPrecision> rho_aux(dim_levels, cPrecision(0, 0));

  double g       = 0.0;
  cPrecision mu1 = 0 * 2.0;
  cPrecision mu2 = 0 * 2.0;
  double lambda  = 0.174388;
  double om1     = 0;
  double om2     = 0.01;

  rho_aux[0] = (cPrecision(0, -1.0)) * EMFvalue * rhos[1] +
               cPrecision(0, 1.0) * conj(EMFvalue) * conj(rhos[1]) +
               g * rhos[2];
  rho_aux[1] = (cPrecision(0, -1.0)) * conj(EMFvalue) * rhos[0] +
               (-g / 2 + cPrecision(0, 1.0) * (-om1 + om2)) * rhos[1] +
               cPrecision(0, 1.0) * conj(EMFvalue) * rhos[2];
  rho_aux[2] = cPrecision(0, 1.0) * EMFvalue * rhos[1] -
               cPrecision(0, 1.0) * conj(EMFvalue) * conj(rhos[1]) -
               g * rhos[2];

  return rho_aux;
}