#pragma once

#include "Cell_system_node.h"

double envelope_neg(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_0) / wx, 2) + pow((y - y_0) / wy, 2) + 0 * pow((z - z_0) / wz, 2)));
}

double envelope_pos(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_1) / wx, 2) + pow((y - y_1) / wy, 2) + 0 * pow((z - z_1) / wz, 2)));
}

double envelope_double(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_2) / wx, 2) + pow((y - y_2) / wy, 2) + 0 * pow((z - z_2) / wz, 2)));
}

int strong_scaling(string saveDir, int nodes_x)
{
  boost::filesystem::create_directory(saveDir);

  // Dimension of the total system in micrometers
  double Lx = 1.0 * 10e2;
  double Ly = 1.0 * 10e1;
  double Lz = 1.0 * 10e1;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // integration step
  double dt = 0.05;

  // density of the gas
  const double density = 10e-2;  // parts/micrometer**3

  // set a mesh for saving the EMF field
  int Nx = 50;
  int Ny = 50;
  int Nz = 50;

  // create a mesh with simulation parameters
  mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

  cout << "************************\nThe Total Number of Cells is ";
  //////////////////////////////////////////////////
  // initialize the need electric fields
  ElectricField E_p1 = ElectricField(0.0000005, -0.01, "E_p1", envelope_pos);
  ElectricField E_p2 = ElectricField(0.0000005, 0.01, "E_p2", envelope_neg);

  vector<ElectricField> EMFs;
  EMFs.push_back(E_p1);
  EMFs.push_back(E_p2);

  ///////////////////////////////////////////////

  // parameters for the simulation - machine related
  int nodes_X = nodes_x;
  int nodes_Y = 1;
  int nodes_Z = 1;

  int NumParticles = density * Lx * Ly * Lz;

  int Ncells_X_node = int(2*160 / nodes_x);
  int Ncells_Y_node = 5;
  int Ncells_Z_node = 5;

  int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node;

  // dimensions of the cell in micrometers
  double Cell_lx = Lx / nodes_X / Ncells_X_node;
  double Cell_ly = Ly / nodes_Y / Ncells_Y_node;
  double Cell_lz = Lz / nodes_Z / Ncells_Z_node;

  // number of particles
  const int NPartPerCell = int(density * Cell_lx * Cell_ly * Cell_lz);

  cout << "************************\nThe Total Number of Cells is "
       << TotalCells << "\nand " << NumParticles << " particles total in system"
       << "\nand " << NPartPerCell << " particles in each cell \n";

  // create the cell system in 1 node
  Cell_system_node total_cells = Cell_system_node(nodes_X,
                                                  nodes_Y,
                                                  nodes_Z,
                                                  Ncells_X_node,
                                                  Ncells_Y_node,
                                                  Ncells_Z_node,
                                                  Cell_lx,
                                                  Cell_ly,
                                                  Cell_lz,
                                                  dt,
                                                  my_mesh,
                                                  EMFs,
                                                  NPartPerCell,
                                                  NLEVELS,
                                                  true,
                                                  1);

  cout << "Cells done\n";

  // start a timer
  auto start = std::chrono::system_clock::now();

  // run the simulation
  int NumIterations = 2;
  total_cells.run(NumIterations, 20, dt, "datat//", "short_range", false);

  // end the timer
  auto end = std::chrono::system_clock::now();
  auto elapsed =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  cout << " Nprocesses " << nodes_x;
  cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

  double elap_max;
  double elap(elapsed.count());

  MPI_Reduce(&elap, &elap_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);


  if (rank == 0) {
    if (nodes_x == 1) {
      std::ofstream param(saveDir +
                          std::string("strongscaling" + std::string(".dat")));

      param << "Nparts\n";
      param << NumParticles;
      param << "\n";
      param << "Ncells\n";
      param << TotalCells;

      param << "Number of processes\t";
      param << "Time elapsed\n";
      param << nodes_x << "\t" << elap_max << "\n";
      param.close();

    } else {
      std::ofstream param(
          saveDir + std::string("strongscaling" + std::string(".dat")),
          std::ios::app);

      // param << "Ncells\n";
      // param << nodes_x;
      // param << "\n";

      param << "Number of processes\t";
      param << "Time elapsed\n";
      param << nodes_x << "\t" << elap_max << "\n";
      param.close();
    }
  }

  return 0;
}

int weak_scaling(string saveDir, int nodes_x)
{
  boost::filesystem::create_directory(saveDir);

  // Dimension of the total system in micrometers
  double Lx = 1.0 * 10e2;
  double Ly = 1.0 * 10e1;
  double Lz = 1.0 * 10e1;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // integration step
  double dt = 0.05;

  // density of the gas
  const double density = 10e-3;  // parts/micrometer**3

  // set a mesh for saving the EMF field
  int Nx = 50;
  int Ny = 50;
  int Nz = 50;

  // create a mesh with simulation parameters
  mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

  cout << "************************\nThe Total Number of Cells is ";
  //////////////////////////////////////////////////
  // initialize the need electric fields
  ElectricField E_p1 = ElectricField(0.0000005, -0.01, "E_p1", envelope_pos);
  ElectricField E_p2 = ElectricField(0.0000005, 0.01, "E_p2", envelope_neg);

  vector<ElectricField> EMFs;
  EMFs.push_back(E_p1);
  EMFs.push_back(E_p2);

  ///////////////////////////////////////////////

  // parameters for the simulation - machine related
  int nodes_X = nodes_x;
  int nodes_Y = 1;
  int nodes_Z = 1;

  int NumParticles = density * Lx * Ly * Lz;

  int Ncells_X_node = int(2*16);
  int Ncells_Y_node = 5;
  int Ncells_Z_node = 5;

  int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node;

  // dimensions of the cell in micrometers
  double Cell_lx = Lx / Ncells_X_node;
  double Cell_ly = Ly / Ncells_Y_node;
  double Cell_lz = Lz / Ncells_Z_node;

  // number of particles
  const int NPartPerCell = int(density * Cell_lx * Cell_ly * Cell_lz);

  cout << "************************\nThe Total Number of Cells is "
       << TotalCells << "\nand " << NumParticles << " particles total in system"
       << "\nand " << NPartPerCell << " particles in each cell \n";

  // create the cell system in 1 node
  Cell_system_node total_cells = Cell_system_node(nodes_X,
                                                  nodes_Y,
                                                  nodes_Z,
                                                  Ncells_X_node,
                                                  Ncells_Y_node,
                                                  Ncells_Z_node,
                                                  Cell_lx,
                                                  Cell_ly,
                                                  Cell_lz,
                                                  dt,
                                                  my_mesh,
                                                  EMFs,
                                                  NPartPerCell,
                                                  NLEVELS,
                                                  true,
                                                  1);

  cout << "Cells done\n";

  // start a timer
  auto start = std::chrono::system_clock::now();

  // run the simulation
  int NumIterations = 5;
  total_cells.run(NumIterations, 20, dt, "datat//", "short_range", false);

  // end the timer
  auto end = std::chrono::system_clock::now();
  auto elapsed =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  cout << " Nprocesses " << nodes_x;
  cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

  double elap_max;
  double elap(elapsed.count());

  MPI_Reduce(&elap, &elap_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);


  if (rank == 0) {
    if (nodes_x == 1) {
      std::ofstream param(saveDir +
                          std::string("weakscaling" + std::string(".dat")));

      param << "Nparts\n";
      param << NumParticles;
      param << "\n";
      param << "Ncells\n";
      param << TotalCells;
      param << "\n";

      param << "Number of processes\t";
      param << "Time elapsed\n";
      param << nodes_x << "\t" << elap_max << "\n";
      param.close();

    } else {
      std::ofstream param(
          saveDir + std::string("weakscaling" + std::string(".dat")),
          std::ios::app);

      // param << "Ncells\n";
      // param << nodes_x;
      // param << "\n";

      param << "Number of processes\t";
      param << "Time elapsed\n";
      param << nodes_x << "\t" << elap_max << "\n";
      param.close();
    }
  }

  return 0;
}