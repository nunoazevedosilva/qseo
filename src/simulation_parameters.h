#pragma once

// parameters of the atomic system
const double ARmass  = 100.0;      // 39.948; //A.U.s
const double ARsigma = 3.405;      // Angstroms
const double AReps   = 0 * 119.8;  // Kelvins
