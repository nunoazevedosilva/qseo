#pragma once

#include "mesh.h"
#include "misc.h"

#define DH 0.01  // just to compute the gradient

// class for defining an EMfield
class ElectricField
{
 public:
  double A;     // field amplitude
  double o_p;   // a free parameter, use it as you wish
  string name;  // name for saving the field

  std::function<double(double, double, double, double)>
      envelope;  // envelope function of the electric field

  // Constructor for an ElectricField
  ElectricField(double A,
                double o_p,
                string name,
                std::function<double(double, double, double, double)> envelope)
      : A(A), o_p(o_p), name(name), envelope(envelope){};

  // give the value of the electric field at given point, time
  cPrecision Value(double x, double y, double z, double t)
  {
    return A * envelope(x, y, z, t) *
           (exp(cPrecision(0, 1) * o_p * t) + exp(cPrecision(0, -1) * o_p * t));
  }

  // give the gradient along x at a given point, time
  double gradx(double x, double y, double z, double t)
  {
    return A * (envelope(x, y, z, t) - envelope(x + DH, y, z, t)) / DH;
  }

  // give the gradient along y at a given point, time
  double grady(double x, double y, double z, double t)
  {
    return A * (envelope(x, y, z, t) - envelope(x, y + DH, z, t)) / DH;
  }

  // give the gradient along z at a given point, time
  double gradz(double x, double y, double z, double t)
  {
    return A * (envelope(x, y, z, t) - envelope(x, y, z + DH, t)) / DH;
  }

  // function to save the electromagnetic field
  void save_vtk_field(vtkSmartPointer<vtkStructuredGrid> structuredGrid,
                      mesh mymesh,
                      std::string filename,
                      double time)
  {
    vtkSmartPointer<vtkDoubleArray> emf =
        vtkSmartPointer<vtkDoubleArray>::New();
    emf->SetNumberOfComponents(1);
    // velocity->SetNumberOfTuples(NumParticles);
    emf->SetName("EMF");

    double ms[1];
    for (unsigned int k = 0; k < mymesh.Nz; k++) {
      for (unsigned int j = 0; j < mymesh.Ny; j++) {
        for (unsigned int i = 0; i < mymesh.Nx; i++) {
          ms[0] = envelope(mymesh.x[i], mymesh.y[j], mymesh.z[k], time);
          emf->InsertNextTuple(ms);
        }
      }
    }

    structuredGrid->GetPointData()->AddArray(emf);

    // Write file
    vtkSmartPointer<vtkXMLStructuredGridWriter> writer =
        vtkSmartPointer<vtkXMLStructuredGridWriter>::New();
    writer->SetFileName(filename.c_str());
    writer->SetInputData(structuredGrid);
    writer->Write();
  }
};
