#include "Cell_system_node.h"
#include "tests_scaling.h"

//#include "benchmark_functions.h"

/*
double envelope_neg(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_0) / wx, 2) + pow((y - y_0) / wy, 2) + 0 * pow((z - z_0) / wz, 2)));
}

double envelope_pos(double x, double y, double z, double t)
{
	////////////////////////////////////////////////
	 
	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_1) / wx, 2) + pow((y - y_1) / wy, 2) + 0 * pow((z - z_1) / wz, 2)));
}

double envelope_double(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 4;
	double y_0 = Ly / 4;
	double z_0 = Lz / 4;

	double x_1 = 3 * Lx / 4;
	double y_1 = 3 * Ly / 4;
	double z_1 = 3 * Lz / 4;

	double x_2 = 3 * Lx / 4;
	double y_2 = 1 * Ly / 4;
	double z_2 = 3 * Lz / 4;

	double wx = Lx / 8.;
	double wy = Ly / 8.;
	double wz = Lz / 8.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_2) / wx, 2) + pow((y - y_2) / wy, 2) + 0 * pow((z - z_2) / wz, 2)));
}

/*
int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	int n_threads = 1;
	
	//integration step
	double dt = 0.05;
	//Number of iterations
	int NumIterations = 20;

	//density of the gas
	const double density = 10e-5; //parts/micrometer**3

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;

	//set a mesh for saving the EMF field
	int Nx = 50;
	int Ny = 50;
	int Nz = 50;

	//create a mesh with simulation parameters
	mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

	//////////////////////////////////////////////////
	//initialize the need electric fields
	ElectricField E_p1 = ElectricField(0.5, -0.01,"E_p1", envelope_pos);
	ElectricField E_p2 = ElectricField(0.5, 0.01,"E_p2", envelope_neg);
	ElectricField E_c = ElectricField(0.5,  0.0, "E_p3", envelope_double);

	vector<ElectricField> EMFs;
	EMFs.push_back(E_p1);
	EMFs.push_back(E_p2);
	EMFs.push_back(E_c);

	///////////////////////////////////////////////

	//parameters for the simulation - machine related
	int TotalNodes_X = 2;
	int TotalNodes_Y = 2;
	int TotalNodes_Z = 1;

	int Ncells_X_node = 20;
	int Ncells_Y_node = 20;
	int Ncells_Z_node = 1;

	int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node*TotalNodes_X*TotalNodes_Y*TotalNodes_Z;

	//dimensions of the cell in micrometers
	double Cell_lx = Lx / TotalNodes_X / Ncells_X_node;
	double Cell_ly = Ly / TotalNodes_Y / Ncells_Y_node;
	double Cell_lz = Lz / TotalNodes_Z / Ncells_Z_node;

	//number of particles
	const int NPartPerCell = int(density*Cell_lx*Cell_ly*Cell_lz);
	int NumParticles = density * Lx*Ly*Lz;

	//create the cell system in 1 node
	Cell_system_node total_cells = Cell_system_node(TotalNodes_X, TotalNodes_Y, TotalNodes_Z, Ncells_X_node, Ncells_Y_node, Ncells_Z_node, Cell_lx, Cell_ly, Cell_lz, dt, my_mesh, EMFs, NPartPerCell, NLEVELS, true, n_threads);

	cout << "\nThe Total Number of Cells is " << TotalCells
		<< "\nand " << NumParticles << " particles total in system"
		<< "\nand " << NPartPerCell << " particles in each cell \n";
	
	cout << "Cells done\n";

	//start a timer
	auto start = std::chrono::system_clock::now();

	//run the simulation

	total_cells.run(NumIterations,1,dt,"datatest/","short_range");


	//end the timer
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	cout << endl << "time taken = " << elapsed.count() << "ms"<<'\n';

	//int a = getchar();
	MPI_Finalize();
	return 0;
}

/**/


int main(int argc, char* argv[])
{
	cout << "**********";
	MPI_Init(&argc, &argv);
	cout << "**********";
	int nodes_x;
	MPI_Comm_size(MPI_COMM_WORLD, &nodes_x);
	cout << nodes_x;
	//int a = optimize_n("benchs//", 40, 100, 3);
	//int a = bench_npart_shortrange("benchs//", 0.5, 2.0, 0.3, 67, 8);
	//int a = bench_threads("benchs//", 30, 9);
	int a = strong_scaling("benchs/", nodes_x);  // nodes_x);
	int b = weak_scaling("benchs/", nodes_x);
	MPI_Finalize();
	return 0;
}

/**/
