#pragma once
/*

 QPARTICLES:
 a code to simulate many-body quantum particles in
 interaction with electromagnetic fiels

 Nuno Azevedo Silva
 nuno.a.silva@inesctec.pt

 -------------------------------------------------------

 Cell_system_node.h
 include a class for creating a cell system, containing all the cells in a node

 */


#include "Cell.h"

//// auxiliary struct for retrieving the energy after an update
struct energy
{
	double e_pot;
	double e_kin;
};
////

/**
 * @brief Cell_system_node class
 * Class containing the cell system in a single node
 *
 *
 */
class Cell_system_node {

public:

	vector<vector<vector<Cell>>>  cells;			//a 3D vector of vectors to store each cell data

	int ***CellSizes;								//matrix of the number of particles per cell (you may want different number of particles for different cells)

	int CellSize;									//number of particles per cell if you want a constant one

	int Nx_per_node;								//number of cells in the x direction per node

	int Ny_per_node;								//number of cells in the y direction per node

	int Nz_per_node;								//number of cells in the z direction per node

	double Cell_lx;									//dimensions of a single cell - x direction
	double Cell_ly;									//dimensions of a single cell - y direction
	double Cell_lz;									//dimensions of a single cell - z direction

	double time;									//current simulation time

	int n_threads;									//number of threads used in the processor

	int NPartPerCell;								//particles per cell

	int NumParticles;								//total number of particles in the system

	mesh& pmesh;									//mesh of the system

	vector<ElectricField> EMFs;						//vector with the electric fields

	vtkSmartPointer<vtkStructuredGrid> struct_grid;	//structured grid to save EMF field, to be created using the given mesh

	int Node_x, Node_y, Node_z;						// Position of the node

	map CellMap;									//a 3d vector for a cell map	

	int Rank, NCPUS;								//current rank and total number of cpus

	int TotalNodes_X, TotalNodes_Y, TotalNodes_Z;	//Total number of nodes of the simulation


	//constructor for a cell system to run in a node
	Cell_system_node(int TotalNodes_X, int TotalNodes_Y, int TotalNodes_Z, int Nx_per_node, int Ny_per_node, int Nz_per_node, double Cell_lx, double Cell_ly, double Cell_lz,
		double dt, mesh& pmesh, vector<ElectricField> EMFs, int NPartPerCell = 0, int nlevels = NLEVELS, bool Constant_density = true, int n_threads = 1) :
		TotalNodes_X(TotalNodes_X), TotalNodes_Y(TotalNodes_Y), TotalNodes_Z(TotalNodes_Z), Nx_per_node(Nx_per_node), Ny_per_node(Ny_per_node), Nz_per_node(Nz_per_node),
		Cell_lx(Cell_lx), Cell_ly(Cell_ly), Cell_lz(Cell_lz), n_threads(n_threads), NPartPerCell(NPartPerCell), pmesh(pmesh), EMFs(EMFs)
	{

		//assign rank and total count of cpus
		MPI_Comm_rank(MPI_COMM_WORLD, &Rank);
		MPI_Comm_size(MPI_COMM_WORLD, &NCPUS);
		
		// Print off a hello world message for debugging purposes
		printf("Rank %d out of %d processors\n", Rank, NCPUS);

		//find position in the node system
		Node_x = Rank % (TotalNodes_X*TotalNodes_Y) % TotalNodes_X;
		Node_y = Rank % (TotalNodes_X*TotalNodes_Y) / TotalNodes_X;
		Node_z = Rank / (TotalNodes_X*TotalNodes_Y);


		//create the structured grid
		struct_grid = pmesh.structuredGridtemplate();

		//Compute the number of particles within the node
		NumParticles = NPartPerCell * Nx_per_node*Ny_per_node*Nz_per_node;


		if (Constant_density)
		{
			//use the same number of particles everywhere
			CellSize = NPartPerCell;

			//fill the cell system
			vector<vector<vector<Cell>>> cells2;
			for (int i = 0; i < Nx_per_node; i++) {

				vector<vector<Cell>> row;
				for (int j = 0; j < Ny_per_node; j++) {
					vector<Cell> col;
					for (int k = 0; k < Nz_per_node; k++) {
						//fit the cell in the cell system
						col.push_back(Cell(i, j, k, Node_x, Node_y, Node_z, Nx_per_node, Ny_per_node, Nz_per_node, CellSize, Cell_lx, Cell_ly, Cell_lz, nlevels));
					}
					row.push_back(col);
				}
				cells2.push_back(row);
			}
			cells = cells2;
		}
	
		else{}//still undefined
		/**/

		//Creating a global cell map - rank and total index
		map CellMap_temp;
		for (int i = 0; i < Nx_per_node*TotalNodes_X; i++) {

			vector<vector<vector<int>>> row;
			for (int j = 0; j < Ny_per_node*TotalNodes_Y; j++) {
				vector<vector<int>> col;
				for (int k = 0; k < Nz_per_node*TotalNodes_Z; k++) {

					int super_index = i + j * Nx_per_node*TotalNodes_X + k * Nx_per_node*TotalNodes_X*Ny_per_node*TotalNodes_Y;
					int rr = int(i/Nx_per_node)+int(j/Ny_per_node)*TotalNodes_X+int(k/Nz_per_node)*TotalNodes_X*TotalNodes_Y;
					col.push_back(vector<int> {rr,super_index});
					//cout << i << " - " << j << " - " << k << "--------";
					//cout << "Rank " << rr << " and index" << super_index << endl;
				}
				row.push_back(col);
			}
			CellMap_temp.push_back(row);
		}
		CellMap = CellMap_temp;
	}
	
	//DEPRECATED - update for a long range potential with all-to-all interactions
	energy update_longrange( double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;

	#pragma omp parallel for reduction(+:TotPot) num_threads(n_threads)
	for (int I = 0; I < Nx_per_node; I++)
	{
		for (int J = 0; J < Ny_per_node; J++)
		{
			for (int K = 0; K < Nz_per_node; K++)
			{
				// Consider interactions between particles within the cell

				for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
				{
					
					for (int p2 = 0; p2 < cells[I][J][K].particles.size(); p2++)
					{
						if (p1 != p2)
							//TotPot += interactMS(cells[i][j][k].particles[p1], cells[i][j][k].particles[p2]);
							TotPot += cells[I][J][K].particles[p1].interact_LJ(cells[I][J][K].particles[p2]);
					}
				}


				// Consider each other cell... 
				for (int i2 = 0; i2 < Nx_per_node; i2++)
				{
					for (int j2 = 0; j2 < Ny_per_node; j2++)
					{
						for (int k2 = 0; i2 < Nz_per_node; i2++)
						{
							for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
							{
								//cout << i2 << j2 << k2 << endl;
								for (int p2 = 0; p2 < cells[i2][i2][k2].particles.size(); p2++)
								{
									//uncomment this line
									TotPot += cells[I][J][K].particles[p1].interact_LJ(cells[i2][j2][k2].particles[p2]);
									
								}
							}
						}
					}
				}
			}
		}
	}

		// End iteration over cells    
		// Apply the accumulated forces; update accelerations, velocities, positions

	//#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)

		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						//printf("******** %i %i %i, ")
						
						TotKin += cells[i][j][k].particles[p].update(time, dt);
					}
				}
			}
		}
	

		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	//DEPRECATED - update routine for a short range potential with neighbor cells only
	energy update_shortrange(double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;

	//#pragma omp parallel for reduction(+:TotPot) num_threads(n_threads)
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					// Consider interactions between particles within the cell
					for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
					{
						for (int p2 = 0; p2 < cells[I][J][K].particles.size(); p2++)
						{
							if (p1 != p2)
								//TotPot += cells[I][J][K].particles[p1].interact_LJ(cells[I][J][K].particles[p2]);
								TotPot += cells[I][J][K].particles[p1].interact_MS(cells[I][J][K].particles[p2], time, EMFs[0]);
						}
					}

					// Consider each other cell... 
					for (int i2 = 0; i2 < Nx_per_node; i2++)
					{
						for (int j2 = 0; j2 < Ny_per_node; j2++)
						{
							for (int k2 = 0; i2 < Nz_per_node; i2++)
							{
								for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
								{
									//cout << i2 << j2 << k2 << endl;
									for (int p2 = 0; p2 < cells[i2][i2][k2].particles.size(); p2++)
									{

										// true if is neighbor
										if ((abs(I - i2) < 2) && (abs(J - j2) < 2) && (abs(K - k2) < 2) && (I != i2 || J != j2 || K != k2)) {
											// Consider all interactions between particles in neighbor cells
											for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
											{
												for (int p2 = 0; p2 < cells[i2][j2][k2].particles.size(); p2++)
												{
													//TotPot += cells[I][J][K].particles[p1].interact_LJ(cells[i2][j2][k2].particles[p2]);
													//TotPot += cells[I][J][K].particles[p1].interact_MS(cells[i2][j2][k2].particles[p2], time);
												}
											}
										}
										
									}
								}
							}
						}
					}
				}
			}
		}

		// End iteration over cells    
		// Apply the accumulated forces; update accelerations, velocities, positions

	//#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)

		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						//printf("******** %i %i %i, ")

						TotKin += cells[i][j][k].particles[p].update(time, dt);
					}
				}
			}
		}


		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	//DEPRECATED - routine for a case when there is no interactions
	energy update_nointeraction(double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;

		  
		// update accelerations, velocities, positions
	//#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)
		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						//printf("******** %i %i %i, ")

						TotKin += cells[i][j][k].particles[p].update(time, dt);
					}
				}
			}
		}


		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	//DEPRECATED - update for a long range potential with all-to-all interactions
	energy update_longrange_EMF(double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;

	//#pragma omp parallel for reduction(+:TotPot) num_threads(n_threads)
	//#pragma omp parallel for num_threads(n_threads)
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					// Consider interactions between particles within the cell

					for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
					{

						for (int p2 = 0; p2 < cells[I][J][K].particles.size(); p2++)
						{
							if (p1 != p2)
								//TotPot += interactMS(cells[i][j][k].particles[p1], cells[i][j][k].particles[p2]);
								TotPot += cells[I][J][K].particles[p1].interact_MS(cells[I][J][K].particles[p2], time, EMFs[0]);
						}
					}


					// Consider each other cell... 
					for (int i2 = 0; i2 < Nx_per_node; i2++)
					{
						for (int j2 = 0; j2 < Ny_per_node; j2++)
						{
							for (int k2 = 0; i2 < Nz_per_node; i2++)
							{
								for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
								{
									//cout << i2 << j2 << k2 << endl;
									for (int p2 = 0; p2 < cells[i2][i2][k2].particles.size(); p2++)
									{
										//uncomment this line
										TotPot += cells[I][J][K].particles[p1].interact_MS(cells[i2][j2][k2].particles[p2], time, EMFs[0]);

									}
								}
							}
						}
					}
				}
			}
		}

		// End iteration over cells    
		// Apply the accumulated forces; update accelerations, velocities, positions

//#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)

		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						//printf("******** %i %i %i, ")

						TotKin += cells[i][j][k].particles[p].update(time, dt, EMFs);
					}
				}
			}
		}


		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	//update routine for a short range potential with neighbor cells only
	energy update_shortrange_EMF(double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;
	
	
	// Start the communications going, before running each cell
	//we choose to communicate plane by plane, for simplicity of tracking each cell task
	//and to give a chance to prevent memory errors in the future
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					cells[I][J][K].pack_message();
					cells[I][J][K].Communicate_middle_plane(CellMap, TotalNodes_X*Nx_per_node, TotalNodes_Y*Ny_per_node, TotalNodes_Z*Nz_per_node);
					cells[I][J][K].Communicate_top_plane(CellMap, TotalNodes_X*Nx_per_node, TotalNodes_Y*Ny_per_node, TotalNodes_Z*Nz_per_node);
					cells[I][J][K].Communicate_bottom_plane(CellMap, TotalNodes_X*Nx_per_node, TotalNodes_Y*Ny_per_node, TotalNodes_Z*Nz_per_node);
					//cout << TotalNodes_X * Nx_per_node << TotalNodes_Y * Ny_per_node << TotalNodes_Z * Nz_per_node;
				}
			}
		}

	//#pragma omp parallel for reduction(+:TotPot) num_threads(n_threads)
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					//INTERACTION IN THE SAME CELL

					for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
					{

						for (int p2 = 0; p2 < cells[I][J][K].particles.size(); p2++)
						{
							if (p1 != p2) {}
								TotPot += cells[I][J][K].particles[p1].interact_MS(cells[I][J][K].particles[p2], time, EMFs[0]);
						}
					}


				}
			}
		}

	
		printf("\n	waiting for comms");

		
		// Waiting for communications to end
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					cells[I][J][K].PostWaits_middle_plane();
					cells[I][J][K].PostWaits_top_plane();
					cells[I][J][K].PostWaits_bottom_plane();
				}
			}
		}
		
		
		// INTERACTION BETWEEN THE NEIGHBOOR CELLS
		for (int I = 0; I < Nx_per_node; I++)
		{
			for (int J = 0; J < Ny_per_node; J++)
			{
				for (int K = 0; K < Nz_per_node; K++)
				{
					// a for loop for all the neighboor cells

					//middle plane
					//cout << I << J << K << " send and receive ";
					//cout << cells[I][J][K].nreqs_top_plane << cells[I][J][K].nreqs_middle_plane << cells[I][J][K].nreqs_bottom_plane << "   ";
					//cout << cells[I][J][K].nreqr_top_plane << cells[I][J][K].nreqr_middle_plane << cells[I][J][K].nreqr_bottom_plane << endl;
					for (int n_MP = 0; n_MP < cells[I][J][K].nreqr_middle_plane; n_MP++)
					{
						//if (cells[I][J][K].remote_particles_middle_plane[n_MP][0] != 250)
						//{
						//	cout << I << J << K << " ****************** ";
						//}
						for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
						{
							
								//cout << n_MP << "     " << cells[I][J][K].remote_particles_middle_plane[n_MP][0] << "  "; 
							

							for (int p2 = 0; p2 < cells[I][J][K].remote_particles_middle_plane[n_MP][0]; p2++)
							{
							
								
								TotPot += cells[I][J][K].particles[p1].interact_MS( 
										cells[I][J][K].remote_particles_middle_plane[n_MP][p2*3+1], 
										cells[I][J][K].remote_particles_middle_plane[n_MP][p2 * 3 + 2], 
										cells[I][J][K].remote_particles_middle_plane[n_MP][p2 * 3 + 3],
										time, EMFs[0]);
								
								
								
							}
							
						}
					}
					
					
					//top plane
					for (int n_TP = 0; n_TP < cells[I][J][K].nreqr_top_plane; n_TP++)
					{
						for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
						{

							for (int p2 = 0; p2 < cells[I][J][K].remote_particles_top_plane[n_TP][0]; p2++)
							{
								TotPot += cells[I][J][K].particles[p1].interact_MS(
										cells[I][J][K].remote_particles_top_plane[n_TP][p2 * 3 + 1],
										cells[I][J][K].remote_particles_top_plane[n_TP][p2 * 3 + 2],
										cells[I][J][K].remote_particles_top_plane[n_TP][p2 * 3 + 3],
										time, EMFs[0]);
							}
						}
					}

					//bottom plane
					for (int n_BP = 0; n_BP < cells[I][J][K].nreqr_bottom_plane; n_BP++)
					{
						for (int p1 = 0; p1 < cells[I][J][K].particles.size(); p1++)
						{

							for (int p2 = 0; p2 < cells[I][J][K].remote_particles_bottom_plane[n_BP][0]; p2++)
							{
								
								
								TotPot += cells[I][J][K].particles[p1].interact_MS(
										cells[I][J][K].remote_particles_bottom_plane[n_BP][p2 * 3 + 1],
										cells[I][J][K].remote_particles_bottom_plane[n_BP][p2 * 3 + 2],
										cells[I][J][K].remote_particles_bottom_plane[n_BP][p2 * 3 + 3],
										time, EMFs[0]);
							}
						}
					}
					/**/
					

				}
			}
		}
		

		/**/

		// End iteration over cells    
		// Apply the accumulated forces; update accelerations, velocities, positions

		//#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)
		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						TotKin += cells[i][j][k].particles[p].update(time, dt, EMFs);
					}
				}
			}
		}


		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	//DEPRECATED - update routine for a case when there is no interactions
	energy update_nointeraction_EMF(double dt)
	{
		// For each timestep
		double TotPot = 0.0;
		double TotKin = 0.0;


		// update accelerations, velocities, positions
	#pragma omp parallel for reduction(+:TotKin) num_threads(n_threads)
		for (int i = 0; i < Nx_per_node; i++) {
			for (int j = 0; j < Ny_per_node; j++) {
				for (int k = 0; k < Nz_per_node; k++) {
					for (int p = 0; p < cells[i][j][k].particles.size(); p++)
					{
						//printf("******** %i %i %i, ")

						TotKin += cells[i][j][k].particles[p].update(time, dt, EMFs);
					}
				}
			}
		}


		energy e_it;
		e_it.e_pot = TotKin;
		e_it.e_kin = TotPot;

		return e_it;
	}

	/**
	 * Perform a simulation run with specified parameters
	 *
	 * \param [in] NumIterations	Total number of iterations
	 * \param [in] stride_for_save	Number of iterations before saving the data
	 * \param [in] dt				time step
	 * \param [in] filename			folder to save
	 * \param [in] range			range of the interaction,  default short-range, compute only first neighboors
	 * \param [in] save				true if you want to save the data, default true
	 */
	void run(int NumIterations, int stride_for_save, double dt, std::string filename, std::string range = "short_range", bool save = true)
	{
		//create a directory if it does not exist
		boost::filesystem::create_directory(filename);

		//for each iteration
		for (int it = 1; it < NumIterations+1; it++) 
		{ 
			//simulation time
			time = it*dt;

			energy e_it;
			
			// if there is EMFs
			if (EMFs.size() != 0)
			{
				if (range == "short_range") { e_it = update_shortrange_EMF(dt); }
				if (range == "long_range") { e_it = update_longrange_EMF(dt); }
				if (range == "no_interaction") { e_it = update_nointeraction_EMF(dt); }
			}

			// or if there is no EMFs
			else
			{
				if (range == "short_range") { e_it = update_shortrange(dt); }
				if (range == "long_range") { e_it = update_longrange(dt); }
				if (range == "no_interaction") { e_it = update_nointeraction(dt); }
			}


			printf("\nIteration#%d, ----- %e", it, (e_it.e_pot + e_it.e_kin) / NumParticles);

			// if save (unless it is a performance test it should be true)
			if (save)
			{
				//print something to keep track if its ok
				

				//if it is a step to save
				if (it % stride_for_save == 0)
				{
					//save particles
					save_vtk(filename + "CPU_n" + to_string(Rank) + "_" + "it=" + to_string(it) + ".vtu");
					
					//save each EMF field - not necessary if time-independent
					//save only one copy
					if (Rank == 0)
					{ 
						for (int ff = 0; ff < EMFs.size(); ff++)
						{
							EMFs[ff].save_vtk_field(struct_grid, pmesh, filename + EMFs[ff].name + "_it" + to_string(it) + ".vts", time);
						}

					}
					
				}
			}

		}
	};

	//method to save the current cell as an unstructured grid to vtu file
	void save_vtk(std::string filename)
	{

		//smart pointers for points, the position of each particle
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

		//smart pointer for velocities
		vtkSmartPointer<vtkDoubleArray> velocities = vtkSmartPointer<vtkDoubleArray>::New();
		vtkSmartPointer<vtkDoubleArray> velocity = vtkSmartPointer<vtkDoubleArray>::New();
		velocity->SetNumberOfComponents(3);
		velocity->SetName("Velocity");

		//smart pointer for mass
		vtkSmartPointer<vtkDoubleArray> mass = vtkSmartPointer<vtkDoubleArray>::New();
		mass->SetNumberOfComponents(1);
		mass->SetName("Mass");

		//smart pointer for density operator if it exists
		vtkSmartPointer<vtkDoubleArray> density_op = vtkSmartPointer<vtkDoubleArray>::New();
		density_op->SetNumberOfComponents(3);///DIMLEVELS);
		density_op->SetName("Density Operator");


		// temporary arrays to store information in the for loop
		double d_op[3];//DIMLEVELS];
		double vec[3];
		double ms[1];

		// run through all the cells
		for (int i = 0; i < Nx_per_node; i++)
		{
			for (int j = 0; j < Ny_per_node; j++)
			{
				for (int k = 0; k < Nz_per_node; k++)
				{

					//number of particles in the current cell
					int nparts = cells[i][j][k].particles.size();

					//for each particle
					for (int p = 0; p < nparts; p++)
					{
						//add a point
						points->InsertNextPoint(cells[i][j][k].particles[p].x, cells[i][j][k].particles[p].y, cells[i][j][k].particles[p].z);

						//set the velocity and add it
						vec[0] = cells[i][j][k].particles[p].vx;
						vec[1] = cells[i][j][k].particles[p].vy;
						vec[2] = cells[i][j][k].particles[p].vz;
						velocity->InsertNextTuple(vec);

						//set the mass and add it
						ms[0] = 1;
						mass->InsertNextTuple(ms);

						//if it has a density operator
						if (NLEVELS != 0)
						{
							//set the real part and add it to density_op pointer
							for (int l = 0; l < 3; l++ ) //DIMLEVELS; l++)

							{
								//cout <<endl<<l<<endl<<cells[cx1][cy1].particles[p].rho_p.rhos_j.size() << endl;
								d_op[l] = (cells[i][j][k].particles[p].rho_p.rhos_j[l]).real();
							}
							density_op->InsertNextTuple(d_op);
						}

					}
				}

			}
		}


		//create a pointer for an UnstructuredGrid and add the arrays
		vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
		unstructuredGrid->SetPoints(points);
		unstructuredGrid->GetPointData()->AddArray(velocity);
		unstructuredGrid->GetPointData()->AddArray(mass);
		if (NLEVELS != 0) { unstructuredGrid->GetPointData()->AddArray(density_op); }


		// Write file
		vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
		writer->SetFileName(filename.c_str());
		writer->SetInputData(unstructuredGrid);
		writer->Write();

		/*
		if (Rank == 0)//root
		{
			std::string file_p = filename + ".pvtu";

			auto pwriterObj = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();

			pwriterObj->EncodeAppendedDataOff();
			pwriterObj->SetFileName(file_p.c_str());
			pwriterObj->SetNumberOfPieces(NCPUS);
			pwriterObj->SetInputData(unstructuredGrid);
			pwriterObj->Update();
		}
		/**/
	};

};


