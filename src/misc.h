#pragma once

#include <memory>
#include "definitions.h"

int ComputeAtomsPerCell(int ***sizes,
                        int Nx_per_node,
                        int Ny_per_node,
                        int Nz_per_node,
                        int NPartPerCell)
{
  int max = NPartPerCell;
  for (int i = 0; i < Nx_per_node; i++) {
    for (int j = 0; j < Ny_per_node; j++) {
      for (int k = 0; k < Nz_per_node; k++) {
        cout << endl;
        printf(to_string(i).c_str());
        printf(to_string(j).c_str());
        printf(to_string(k).c_str());
        sizes[i][j][k] = NPartPerCell;
      }
    }
  }

  /* NEED TO FINISH THIS
  int molsum = NR * NCols*NPartPerCell;
  while (molsum < NParts) {
          max++;
          for (int i = 0; i < NRows; i++) {
                  for (int j = 0; j < NCols; j++) {
                          sizes[i][j]++;
                          molsum++;
                          if (molsum >= NParts) {
                                  return max;
                          }
                  }
          }
  }
  */
  return max;
}

////Auxiliary function for getting the corresponding node and cell for a given
/// position
std::shared_ptr<int> find_node_and_cell(double x,
                                        double y,
                                        double z,
                                        double Cell_lx,
                                        double Cell_ly,
                                        double Cell_lz,
                                        int Ncells_X_node,
                                        int Ncells_Y_node,
                                        int Ncells_Z_node)
{
  // first three members are the node, the last three the cell in that node
  std::shared_ptr<int> node_and_cell =
      std::shared_ptr<int>(static_cast<int *>(malloc(sizeof(int) * 6)), free);

  // int* node_and_cell_ = node_and_cell.get();

  // Computing the node
  node_and_cell.get()[0] = int(x / (Cell_lx * Ncells_X_node));
  node_and_cell.get()[1] = int(y / (Cell_ly * Ncells_Y_node));
  node_and_cell.get()[2] = int(z / (Cell_lz * Ncells_Z_node));

  // computing the cell in that node
  node_and_cell.get()[3] =
      int(remainder(x, (Cell_lx * Ncells_X_node)) / Cell_lx);
  node_and_cell.get()[4] =
      int(remainder(y, (Cell_ly * Ncells_Y_node)) / Cell_ly);
  node_and_cell.get()[5] =
      int(remainder(x, (Cell_lz * Ncells_Z_node)) / Cell_lz);

  return node_and_cell;
}

////BLOCH FIELD AUXILIARY FUNCTIONS

// indexes ij to superindex I
int ij_to_I(int i, int j, int levels)
{
  int actual       = 0;
  int ncols_in_row = levels;
  for (int i_n = 0; i_n < i - 1; i_n++) {
    actual += ncols_in_row - i_n;
  }
  return actual + (j - i);
}

// superindex to index i
int I_to_i(int I, int levels)
{
  int i_n = 0;
  int j_n = 0;
  for (int i = 1; i < levels + 1; i++) {
    for (int j = i; j < levels + 1; j++) {
      if (ij_to_I(i, j, levels) == I) {
        i_n = i;
        j_n = j;
      }
    }
  }

  return i_n;
}

// superindex to index j
int I_to_j(int I, int levels)
{
  int i_n = 0;
  int j_n = 0;
  for (int i = 1; i < levels + 1; i++) {
    for (int j = i; j < levels + 1; j++) {
      if (ij_to_I(i, j, levels) == I) {
        i_n = i;
        j_n = j;
      }
    }
  }

  return j_n;
}
