#pragma once

#include "misc.h"

// create a spatial mesh for saving the electric fields
class mesh
{
 public:
  int Nx;  // number of points in the x direction
  int Ny;  // number of points in the y direction
  int Nz;  // number of points in the z direction

  double Lx;  // limit in the x direction
  double Ly;  // limit in the y direction
  double Lz;  // limit in the z direction

  vector<double> x;  // vector with x coordinates;
  vector<double> y;  // vector with y coordinates;
  vector<double> z;  // vector with z coordinates;

  // constructor
  mesh(int Nx, int Ny, int Nz, double Lx, double Ly, double Lz)
      : Nx(Nx), Ny(Ny), Nz(Nz), Lx(Lx), Ly(Ly), Lz(Lz)

  {
    // create the vectors and assign them

    vector<double> xv(Nx);
    for (int i = 0; i < Nx; i++) {
      xv[i] = Lx / Nx * double(i);
    }
    x = xv;

    vector<double> yv(Ny);
    for (int i = 0; i < Ny; i++) {
      yv[i] = Ly / Ny * double(i);
    }
    y = yv;

    vector<double> zv(Nz);
    for (int i = 0; i < Nz; i++) {
      zv[i] = Lz / Nz * double(i);
    }
    z = zv;
  };

  // function to create a structured grid smart pointer for the mesh
  vtkSmartPointer<vtkStructuredGrid> structuredGridtemplate()
  {
    vtkSmartPointer<vtkStructuredGrid> structuredGrid =
        vtkSmartPointer<vtkStructuredGrid>::New();

    // pointer for points of the grid
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

    // for each point of the mesh
    for (unsigned int k = 0; k < Nz; k++) {
      for (unsigned int j = 0; j < Ny; j++) {
        for (unsigned int i = 0; i < Nx; i++) {
          // insert the position
          points->InsertNextPoint(x[i], y[j], z[k]);
        }
      }
    }

    // Specify the dimensions of the grid and set the points
    structuredGrid->SetDimensions(Nx, Ny, Nz);
    structuredGrid->SetPoints(points);

    return structuredGrid;
  }
};
